<?php
include ("models/m_banner.php");
include ("SimpleImage.php");
class c_banner {
    public function show_banner() {
        //xu ly phan danh sach banner
        $m_banner = new m_banner();
        $banners = $m_banner->read_banner();
//        var_dump($banners);
//        die();
        $view = 'view/banner/v_banner.php';
        include 'templates/layout.php';
    }
    public function  add_banner(){
        $m_banner = new  m_banner();
        if (isset($_POST['btnSave']))
        {
            $ma_tieu_de = NULL;
            $ten_tieu_de = $_POST["ten_tieu_de"];
            $trang_thai = $_POST["trang_thai"];
            $hinh_tieu_de = ($_FILES['f_hinh_tieu_de']['error'] == 0) ? $_FILES['f_hinh_tieu_de']['name'] : "";
            $result_insert = $m_banner->insert_banner($ma_tieu_de,$ten_tieu_de,$hinh_tieu_de,$trang_thai);
            if ($result_insert)
            {
                if ($hinh_tieu_de != "")
                {
                    move_uploaded_file($_FILES['f_hinh_tieu_de']['tmp_name'],"../public/layout/imagebanner/$hinh_tieu_de");
                    $image = new  SimpleImage();
                    $dataImage['width'] = 1440;
                    $dataImage['height'] = 585;
                    $dataImage['path'] = "../public/layout/imagebanner";
                    $dataImage['name'] = $hinh_tieu_de;
                    $image->load($dataImage['path'] . '/' . $dataImage['name']);
                    $image->resize($dataImage['width'],$dataImage['height']);
                    $image->save($dataImage['path'] . '/' . $dataImage['name']);
                }
               echo "<script>window.location='banner.php'</script>";

            }
            else
            {
                echo "<script>alert('thêm không thành công')</script>";
            }

        }
        $view = 'view/banner/v_addbanner.php';
        include 'templates/layout.php';
    }
    public function edit_banner(){
        $m_banner = new m_banner();
        if (isset($_GET['ma_tieu_de']))
        {
           $banner_detail = $m_banner->read_banner_by_idbaner($_GET['ma_tieu_de']);
           if(isset($_POST['btnSave']))
           {
               $ma_tieu_de  = $_GET['ma_tieu_de'];
               $ten_tieu_de = $_POST["ten_tieu_de"];
               $trang_thai = $_POST["trang_thai"];
               $hinh = ($_FILES["f_hinh_tieu_de"]['error'] == 0)  ? $_FILES["f_hinh_tieu_de"]["name"] : $banner_detail->hinh;

               $result = $m_banner->edit_banner($ma_tieu_de,$ten_tieu_de,$hinh,$trang_thai);
//               var_dump($result);
//               die();
               if($result)
               {
                   if($_FILES["f_hinh_tieu_de"]["error"] == 0)
                   {
                       //uphinh
                       //hành động đẩy hỉnh vào folder
                       move_uploaded_file($_FILES['f_hinh_tieu_de']['tmp_name'],"../public/layout/imagebanner/$hinh");
                       $image = new  SimpleImage();
                       $dataImage['width'] = 1440;
                       $dataImage['height'] = 585;
                       $dataImage['path'] = "../public/layout/imagebanner";
                       $dataImage['name'] = $hinh;
                       $image->load($dataImage['path'] . '/' . $dataImage['name']);
                       $image->resize($dataImage['width'],$dataImage['height']);
                       $image->save($dataImage['path'] . '/' . $dataImage['name']);
                   }

                   echo "<script>window.location='banner.php'</script>";
               } else {
                   echo "<script>alert('cập nhật không thành công')</script>";
               }

           }
        }
        $view = 'view/banner/v_editbanner.php';
        include 'templates/layout.php';

        //camelCase (tên biến)
        //PasCal // Đặt cho các class ví dụ
        ///HomeController

    }
}