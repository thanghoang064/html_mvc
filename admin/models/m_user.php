<?php
include_once ("database.php");
class m_user extends database {
    public function read_user()
    {
        $sql = "select * from nguoi_dung";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    // để kiểm tra tài khoản và mật khẩu
    public function read_user_by_id_pass($user_name,$password)
    {
//        $user_name = "'phuongttttt' or 1=1";
        $sql = "select * from nguoi_dung where ten_dang_nhap = ? and mat_khau = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($user_name,md5($password)));
            //Lỗ hổng bảo mật SQL inject
        // giải pháp giấu input đi
    }

    public function read_user_by_username($username)
    {
        $sql = "select * from nguoi_dung where ten_dang_nhap = ?";
        $this->setQuery($sql);
        $this->loadRow(array($username));
    }

}