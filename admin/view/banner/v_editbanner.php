<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form class="form-horizontal" enctype="multipart/form-data" method="post">
                        <div class="card-body">
                            <h4 class="card-title">Sửa tiêu đề </h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên tiêu đề</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="ten_tieu_de"  name="ten_tieu_de" value="<?php echo $banner_detail->ten_tieu_de;?>"  placeholder="First Name Here">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Hình</label>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="f_hinh_tieu_de" name="f_hinh_tieu_de" >
                                        <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                        <img src="../public/layout/imagebanner/<?php echo $banner_detail->hinh ?>" width="180px">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                <div class="col-sm-9">
                                    <select id="trang_thai" name="trang_thai">
                                        <option value="0" <?php echo $banner_detail->trang_thai == 0 ?"selected" :""?>>Khóa</option>
                                        <option value="1"  <?php echo $banner_detail->trang_thai == 1 ?"selected" :""?>>Mở</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary" style="margin-left: 40%;" id="" name="btnSave">Lưu</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
        <!-- editor -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>