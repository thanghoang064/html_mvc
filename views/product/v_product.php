<!-- main-area -->
<main>

    <!-- breadcrumb-area -->
    <?php
        include ("v_breadcrumb_area.php");
    ?>
    <!-- breadcrumb-area-end -->

    <!-- shop-area -->
    <section class="shop-area pt-100 pb-100">
        <div class="container">
            <div class="row">
                <?php
                include ("v_item_main.php");
                include ("v_item_top.php");
                ?>
            </div>
        </div>
    </section>
    <!-- shop-area-end -->

</main>
<!-- main-area-end -->