<div class="col-xl-9 col-lg-8">
    <div class="shop-top-meta mb-35">
        <div class="row">
            <div class="col-md-6">
                <div class="shop-top-left">
                    <ul>
                        <li><a href="#"><i class="flaticon-menu"></i> FILTER</a></li>
                        <li>Showing 1–9 of 80 results</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="shop-top-right">
                    <form action="#">
                        <select name="select">
                            <option value="">Sort by newness</option>
                            <option>Free Shipping</option>
                            <option>Best Match</option>
                            <option>Newest Item</option>
                            <option>Size A - Z</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php
            foreach ($products as $product)
            {
        ?>
        <div class="col-xl-4 col-sm-6">
            <div class="new-arrival-item text-center mb-50">
                <div class="thumb mb-25">
                    <a href="shop-details.html"><img src="public/layout/img/product/n_arrival_product01.jpg" alt=""></a>
                    <div class="product-overlay-action">
                        <ul>
                            <li><a href="cart.html"><i class="far fa-heart"></i></a></li>
                            <li><a href="shop-details.html"><i class="far fa-eye"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="content">
                    <h5><a href="shop-details.html"><?php echo $product->ten_san_pham;?></a></h5>
                    <span class="price"><?php echo number_format($product->don_gia);?></span>
                </div>
            </div>
        </div>
        <?php
            }
        ?>
    </div>
    <div class="pagination-wrap">
        <ul>
            <li class="prev"><a href="#">Prev</a></li>
            <li><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">10</a></li>
            <li class="next"><a href="#">Next</a></li>
        </ul>
    </div>
</div>